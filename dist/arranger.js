(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

/**
 * Concrete instance of GroupingRulesStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
 */
var GroupingRulesStrategy,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

GroupingRulesStrategy = require('./groupingrules__strategy');

window.ngk.plugins.TileArrangement.ConcreteGroupingRulesStrategy = (function(_super) {
  __extends(ConcreteGroupingRulesStrategy, _super);

  function ConcreteGroupingRulesStrategy() {
    return ConcreteGroupingRulesStrategy.__super__.constructor.apply(this, arguments);
  }

  return ConcreteGroupingRulesStrategy;

})(GroupingRulesStrategy);

module.exports = window.ngk.utils.ConcreteGroupingRulesStrategy;


},{"./groupingrules__strategy":5}],2:[function(require,module,exports){

/**
 * Concrete instance of LayoutStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var LayoutStrategy,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

LayoutStrategy = require('./layout__strategy');

window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy = (function(_super) {
  __extends(ConcreteLayoutStrategy, _super);

  function ConcreteLayoutStrategy() {
    return ConcreteLayoutStrategy.__super__.constructor.apply(this, arguments);
  }

  return ConcreteLayoutStrategy;

})(LayoutStrategy);

module.exports = window.ngk.utils.ConcreteLayoutStrategy;


},{"./layout__strategy":7}],3:[function(require,module,exports){

/**
 * The purpose of this class is to be the client for our grouping strategy
 * pattern.
 *
 * This is gonna be a singleton since one grouping strategy can be applied
 * at a time
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var Grouper;

Grouper = (function() {
  var instance;

  function Grouper() {}

  instance = null;

  Grouper = (function() {
    function Grouper(strategy) {
      _.bindAll(this, 'getOrderedRow', 'setStrategy');
      this.strategy = strategy;
    }

    Grouper.prototype.getOrderedRow = function() {
      return this.strategy.buildGroupingRules();
    };

    Grouper.prototype.setStrategy = function(strategy) {
      return this.strategy = strategy;
    };

    return Grouper;

  })();

  Grouper.getInstance = function(strategy) {
    if (instance != null) {
      instance.setStrategy(strategy);
      return instance;
    } else {
      return instance = new Grouper(strategy);
    }
  };

  return Grouper;

})();

module.exports = Grouper;


},{}],4:[function(require,module,exports){

/**
 * Factory class for fillingrules objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var GroupingRulesFactory;

GroupingRulesFactory = (function() {
  var GroupingRulesFactorySingleton, instance;

  function GroupingRulesFactory() {}

  instance = null;

  GroupingRulesFactorySingleton = (function() {
    function GroupingRulesFactorySingleton() {
      _.bindAll(this, 'setStrategies', 'createGroupingRules');
      this.strategy = null;
    }


    /**
     * public method to set the group of strategies that are gonna be used by the library
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[GroupingRuleStrategy]} strategy
     * @param  {[String]} platform
     */

    GroupingRulesFactorySingleton.prototype.setStrategies = function(strategy) {
      this.strategy = strategy;
      return this;
    };


    /**
     * [createGroupingRules description]
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} firstElementPoints
     * @param  {[Array]} row
     * @return {[GroupingRuleStrategy]}
     */

    GroupingRulesFactorySingleton.prototype.createGroupingRules = function() {
      if (this.strategy != null) {
        return new this.strategy();
      } else {
        throw new Error("You need to set a grouping strategy before you can use it.");
      }
    };

    return GroupingRulesFactorySingleton;

  })();

  GroupingRulesFactory.getInstance = function() {
    return instance != null ? instance : instance = new GroupingRulesFactorySingleton();
  };

  return GroupingRulesFactory;

})();

module.exports = GroupingRulesFactory;


},{}],5:[function(require,module,exports){

/**
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
 */
var GroupingRulesStrategy, Utils;

Utils = require('./utils');

GroupingRulesStrategy = (function() {
  function GroupingRulesStrategy() {
    _.bindAll(this, 'buildGroupingRules', 'setRules', 'setDesktopRules', 'setMobileRules', '_grabElement', 'setCurrentRowValues');
  }

  GroupingRulesStrategy.prototype.setCurrentRowValues = function(options) {
    this.population = options.population;
    this.remainingPoints = options.remainingPoints;
    this.row = options.row;
    this.wildcardLimitPerRow = options.wildcardLimitPerRow;
    return this.orderedRow = [];
  };


  /**
   * This method is gonna be used by the client (in the strategy pattern) to
   * run the primary algorithms that applies the ordering logic
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[type]}
   */

  GroupingRulesStrategy.prototype.buildGroupingRules = function() {
    return this.setRules();
  };

  GroupingRulesStrategy.prototype.setRules = function() {
    if (Utils.isMobile()) {
      return this.setMobileRules();
    } else {
      return this.setDesktopRules();
    }
  };


  /**
   * Abstract method that is gonna be implemented by the subclass
   * @author Francisco Ramini <francisco.ramini at globant.com>
   */

  GroupingRulesStrategy.prototype.setDesktopRules = function() {
    throw new Error("[Class GroupingRulesStrategy] You must override setDesktopRules() in your implementing class in order to provide grouping rules for desktop");
  };


  /**
   * Abstract method that is gonna be implemented by the subclass
   * @author Francisco Ramini <francisco.ramini at globant.com>
   */

  GroupingRulesStrategy.prototype.setMobileRules = function() {
    throw new Error("[Class GroupingRulesStrategy] You must override setMobileRules() in your implementing class in order to provide grouping rules for mobile devices");
  };


  /**
   * get an element and delete it from the selected collection
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[String]} index
   * @return {[Object]} item
   */

  GroupingRulesStrategy.prototype._grabElement = function(index) {
    var item;
    item = this.population[index][0];
    this.population[index] = this.population[index].slice(1);
    return item;
  };

  return GroupingRulesStrategy;

})();

module.exports = GroupingRulesStrategy;


},{"./utils":10}],6:[function(require,module,exports){

/**
 * Factory class for layout objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var LayoutStrategyFactory;

LayoutStrategyFactory = (function() {
  var LayoutStrategyFactorySingleton, instance;

  function LayoutStrategyFactory() {}

  instance = null;

  LayoutStrategyFactorySingleton = (function() {
    function LayoutStrategyFactorySingleton() {
      _.bindAll(this, 'setStrategies', 'createLayout');
      this.strategies = {};
    }


    /**
     * public method to set the group of strategies that are gonna be used by the library
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} strategies
     */

    LayoutStrategyFactorySingleton.prototype.setStrategies = function(strategies) {
      if (_.isArray(strategies)) {
        _.each(strategies, (function(_this) {
          return function(strategyObj, index) {
            return _this.strategies[strategyObj.points] = strategyObj.layoutClass;
          };
        })(this));
      } else {
        throw new Error("The param passed to [LayoutStrategyFactory].setStrategies() must be an array");
      }
      return this;
    };


    /**
     * [createLayout description]
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} firstElementPoints
     * @param  {[Array]} row
     * @return {[LayoutStrategy]}
     */

    LayoutStrategyFactorySingleton.prototype.createLayout = function(firstElementPoints, row) {
      var layoutStrategy;
      if (this.strategies[firstElementPoints]) {
        layoutStrategy = new this.strategies[firstElementPoints]();
        layoutStrategy.init(row);
        return layoutStrategy;
      } else {
        throw new Error("The layout with points: " + firstElementPoints + " is not defined.");
      }
    };

    return LayoutStrategyFactorySingleton;

  })();

  LayoutStrategyFactory.getInstance = function() {
    return instance != null ? instance : instance = new LayoutStrategyFactorySingleton();
  };

  return LayoutStrategyFactory;

})();

module.exports = LayoutStrategyFactory;


},{}],7:[function(require,module,exports){

/**
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
 */
var LayoutStrategy, Utils;

Utils = require('./utils');

LayoutStrategy = (function() {
  function LayoutStrategy(row) {
    _.bindAll(this, 'getLayout', '_buildLayoutRules', 'init', 'setRules', 'buildLayout');
  }

  LayoutStrategy.prototype.init = function(row) {
    this._mobileRules = {};
    this._desktopRules = {};
    this._rowDesktopRules = {};
    this._rowMobileRules = {};
    this.currentRules = [];
    this.currentRowRules = [];
    this._rowElements = row;
    this._generalTileOptions = {
      classes: "tile--dynamic"
    };
    return this.rowLength = this._rowElements.length;
  };


  /**
   * this method is used by our client class (strategy pattern) to call
   * all the needed method to create the layoyt
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Object]}
   */

  LayoutStrategy.prototype.buildLayout = function() {
    this.setRules();
    this.currentRules = this._setLayout(this._rowElements, this.currentRules);
    return this.getLayout();
  };


  /**
   * set the rules for the corresponding platform (mobile or desktop)
   * @author Francisco Ramini <francisco.ramini at globant.com>
   */

  LayoutStrategy.prototype.setRules = function() {
    var device;
    if (Utils.isMobile()) {
      this.setMobileRules();
      device = '_mobileRules';
    } else {
      this.setDesktopRules();
      device = '_desktopRules';
    }
    this._buildLayoutRules(0, device);
    return this;
  };


  /**
   * abstract method that will be overriden by the subclasses and
   * will allow us to set all the filling rules for mobile
   * @author Francisco Ramini <francisco.ramini at globant.com>
   */

  LayoutStrategy.prototype.setMobileRules = function() {
    throw new Error("[Class LayoutStrategy] You must override setMobileRules() in your implementing class in order to provide grouping rules for desktop");
  };


  /**
   * abstract method that will be overriden by the subclasses and
   * will allow us to set all the filling rules for desktop
   * @author Francisco Ramini <francisco.ramini at globant.com>
   */

  LayoutStrategy.prototype.setDesktopRules = function() {
    throw new Error("[Class LayoutStrategy] You must override setDesktopRules() in your implementing class in order to provide grouping rules for desktop");
  };


  /**
   * This method returns an object containing the rules for each tile
   * and another object containing the rules to be applied in the row
   * container
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Object]}
   */

  LayoutStrategy.prototype.getLayout = function() {
    return {
      tileRules: this.currentRules,
      rowRules: this._rowDesktopRules
    };
  };


  /**
   * recursive method to alter the object representing a tile with the properties that
   * will be used to generate the layout (used in the related template files)
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[type]} iteration = 0
   * @return {[type]}
   */

  LayoutStrategy.prototype._buildLayoutRules = function(iteration, device) {
    var baseObj;
    if (iteration == null) {
      iteration = 0;
    }
    baseObj = {
      _domArrangement: {}
    };
    baseObj._domArrangement = _.extend(baseObj._domArrangement, _.isArray(this[device]) ? this[device][iteration] : this[device]);
    this.currentRules.push(baseObj);
    if (iteration < this.rowLength - 1) {
      iteration++;
      return this._buildLayoutRules(iteration, device);
    }
  };


  /**
   * this method will be in charge of adding the different layout styles (i.e number of columns,
   * size element, etc) to each tile
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} row
   * @param  {[Array/Object]} tileProps
   * @param  {[Number]} iteration = 0
   * @return {[Array]} rows
   */

  LayoutStrategy.prototype._setLayout = function(row, tileProps, iteration) {
    if (iteration == null) {
      iteration = 0;
    }
    _.extend(row[iteration], _.isArray(tileProps) ? tileProps[iteration] : tileProps, {
      _generalOptions: this._generalTileOptions
    });
    if (iteration < row.length - 1) {
      iteration++;
      return this._setLayout(row, tileProps, iteration);
    } else {
      return row;
    }
  };

  return LayoutStrategy;

})();

module.exports = LayoutStrategy;


},{"./utils":10}],8:[function(require,module,exports){

/**
 * The purpose of this class is to be the client for our strategy
 * pattern
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var Layouter;

Layouter = (function() {
  function Layouter(strategy) {
    _.bindAll(this, 'getLayout');
    this.strategy = strategy;
  }

  Layouter.prototype.getLayout = function() {
    return this.strategy.buildLayout();
  };

  return Layouter;

})();

module.exports = Layouter;


},{}],9:[function(require,module,exports){

/**
 * The purpose of this class is to arrange a group of tiles
 * in rows according to a set of styling rules defined by the
 * design team, that allows each row to have its own layout.
 * @author Francisco Ramini <francisco.ramini at globant.com>
 */
var Grouper, GroupingRulesFactory, LayoutStrategyFactory, Layouter;

Grouper = require('./grouper');

LayoutStrategyFactory = require('./layout__factory');

GroupingRulesFactory = require('./groupingrules__factory');

Layouter = require('./layouter');


/**
 * to avoid the consumer of this library to use the it as a module
 * we'll expose the main class as part of the window obj
 */

window.ngk.plugins.TileArrangement = (function() {

  /**
   * constructor method
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} elems   = {}
   * @param  {[Object]} options = {}
   */
  function TileArrangement(elems, options) {
    if (elems == null) {
      elems = {};
    }
    if (options == null) {
      options = {};
    }
    _.bindAll(this, 'arrange', '_arrangeRow', '_grabFirstElementRow', '_completeRowSpace', '_grabElement', '_maxWildcardsPerRow', '_doDomArrangement', '_addDomFillingOptions', '_findRandomWildcardLimitPerRow', 'init', '_groupPopulation', '_createCateogoryPointsArray', 'setNewCollection', 'generateViews');
    this.opt = {};
    if (!_.isEmpty(elems)) {
      this.init(elems, options);
    }
  }

  TileArrangement.prototype.init = function(elems, options) {
    var defaults;
    defaults = {
      wildcardLimitPerRow: 3,
      stepPoints: 25,
      totalPointsPerRow: 100
    };
    _.extend(this.opt, defaults, options);
    this.elems = elems;
    this.elemsCpy = this.elems;
    this.rows = [];
    this.layouts = {};
    this.layoutFactory = LayoutStrategyFactory.getInstance();
    this.layoutFactory.setStrategies(this.opt.layoutStrategies);
    this.groupingFactory = GroupingRulesFactory.getInstance();
    if (!!this.opt.groupingStrategy) {
      this.groupingFactory.setStrategies(this.opt.groupingStrategy);
    } else {
      throw new Error("The option groupingStrategy is mandatory");
    }
    this.groupingObj = this.groupingFactory.createGroupingRules();
    this.grouper = Grouper.getInstance(this.groupingObj);
    this.population = {};
    if (!!this.opt.tileTypes.length) {
      this._createCateogoryPointsArray();
      return this._groupPopulation();
    } else {
      throw new Error("The option tileTypes is mandatory");
    }
  };


  /**
   * creates an ordered array with the elements passed in the tileTypes
   * options
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Array]}
   */

  TileArrangement.prototype._createCateogoryPointsArray = function() {
    this.cateogoryPointsArray = [];
    _.each(this.opt.tileTypes, (function(_this) {
      return function(element, index, list) {
        return _this.cateogoryPointsArray.push(element.points);
      };
    })(this));
    this.cateogoryPointsArray.push('wildcard');
    return this.cateogoryPointsArray.sort(function(a, b) {
      return b - a;
    });
  };


  /**
   * set a new collection after the library has been initialized
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} collection
   * @param  {[Object]} options    = {}
   */

  TileArrangement.prototype.setNewCollection = function(collection, options) {
    if (options == null) {
      options = {};
    }
    return this.init(collection, options);
  };


  /**
   * Recursive method that is used to initialize the population object that
   * is used to hold the member of each of the passed categories
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Number]} iteration = 0
   */

  TileArrangement.prototype._groupPopulation = function(iteration) {
    var groupSettings, items;
    if (iteration == null) {
      iteration = 0;
    }
    groupSettings = this.opt.tileTypes[iteration];
    if (this.population.hasOwnProperty(groupSettings.points)) {
      items = _.where(this.elemsCpy, {
        type: groupSettings.category
      });
      _.each(items, (function(_this) {
        return function(element, index) {
          return _this.population[groupSettings.points].push(element);
        };
      })(this));
    } else {
      this.population[groupSettings.points] = _.where(this.elemsCpy, {
        type: groupSettings.category
      });
    }
    this.elemsCpy = _.difference(this.elemsCpy, this.population[groupSettings.points]);
    iteration++;
    if (!!this.opt.tileTypes[iteration]) {
      return this._groupPopulation(iteration);
    } else {
      if (this.population.hasOwnProperty('wildcard')) {
        return _.each(this.elemsCpy, (function(_this) {
          return function(element, index) {
            return _this.population['wildcard'].push(element);
          };
        })(this));
      } else {
        return this.population['wildcard'] = this.elemsCpy;
      }
    }
  };


  /**
   * this method will be in charge of ordering a group of tiles and will
   * be the "public" method that needs to be called whenever we need to
   * to order a group of tiles
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Object]} arrays
   */

  TileArrangement.prototype.arrange = function() {
    var _rows;
    _rows = [];
    _rows = this._arrangeRow();
    _rows = this._doDomArrangement(_rows);
    this.rows = _rows;
    return this.rows;
  };


  /**
   * [applyRowOrderingLogic description]
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Function]} orderingLogic [description]
   * @param  {[Array]} rows          [description]
   * @return {[Array]}               [description]
   */

  TileArrangement.prototype.applyRowOrderingLogic = function(orderingLogic, rows) {
    if (_.isFunction(orderingLogic)) {
      return orderingLogic(rows);
    } else {
      throw new Error("The first parameter to [tileArrangement].applyRowOrderingLogic must be a function");
    }
  };


  /**
   * [generateViews description]
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} orderedRows
   * @param  {[BackboneView]} RowContainer
   * @return {[type]}
   */

  TileArrangement.prototype.generateViews = function(orderedRows, RowContainer) {
    return _.each(orderedRows, (function(_this) {
      return function(row, index) {
        var rc;
        rc = new RowContainer({
          model: {
            styles: row.props[0]
          }
        });
        _.each(row.row, function(tile, tileIndex) {
          var tileType, tileView;
          tileType = _.find(_this.opt.tileTypes, function(t) {
            return tile.type === t.category;
          });
          if (typeof tileType === "undefined") {
            tileView = new _this.opt.defaultView();
          } else {
            tileView = new tileType.viewName();
          }
          tileView.model = _.extend(tile, {
            cid: tileView.cid
          });
          return rc.setSubViews(tileView.render().el);
        });
        return rc.render();
      };
    })(this));
  };


  /**
   * this method is in charge of adding the necesary options on each object (tile)
   * to correctly display the row in the browser
   * The returning array return an object with the row and the props that needs to be applied
   * to the container of the row
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} tilesCollection
   * @return {[Object]} rows
   */

  TileArrangement.prototype._doDomArrangement = function(tilesCollection) {
    var rowProps, rows;
    rowProps = [];
    rows = [];
    _.each(tilesCollection, (function(_this) {
      return function(row, rowNum) {
        var result;
        result = _this._addDomFillingOptions(row);
        return rows.push({
          row: result.row,
          props: result.rowProps
        });
      };
    })(this));
    return rows;
  };


  /**
   * this method will be in charge of adding the corresponding
   * properties (css options mostly) to the elems that will allow them to be displayed
   * correctly in the browser
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Array]} row
   */

  TileArrangement.prototype._addDomFillingOptions = function(row) {
    var firstElement, firstElementPoints, layoutObj, layoutRules, layouter, rowLevelProps, rowProps, tileProps;
    rowLevelProps = [];
    firstElement = row[0];
    firstElementPoints = firstElement._strengthPoints;
    layoutObj = this.layoutFactory.createLayout(firstElementPoints, row);
    layouter = new Layouter(layoutObj);
    layoutRules = layouter.getLayout();
    tileProps = layoutRules.tileRules;
    rowProps = layoutRules.rowRules;
    rowLevelProps.push(rowProps);
    return {
      row: tileProps,
      rowProps: rowLevelProps
    };
  };


  /**
   * recursive method to iterate over all of the collections an arrange them
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Array]} rows
   */

  TileArrangement.prototype._arrangeRow = function(rows, iteration) {
    var firstElement, row, _rows;
    if (rows == null) {
      rows = [];
    }
    if (iteration == null) {
      iteration = 0;
    }
    _rows = rows || [];
    firstElement = this._grabFirstElementRow();
    if (firstElement) {
      row = this._completeRowSpace(firstElement);
      _rows.push(row);
      iteration++;
      return this._arrangeRow(_rows, iteration);
    } else {
      return _rows;
    }
  };


  /**
   * grab (get and erase an element from a collection) the first element for the row
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {[Object]}
   */

  TileArrangement.prototype._grabFirstElementRow = function(iteration) {
    var item;
    if (iteration == null) {
      iteration = 0;
    }
    if (!!this.population[this.cateogoryPointsArray[iteration]].length) {
      item = this._grabElement(this.cateogoryPointsArray[iteration]);
      this._tagElement(item, this.cateogoryPointsArray[iteration]);
      return item;
    } else {
      iteration++;
      if (iteration < this.cateogoryPointsArray.length) {
        return this._grabFirstElementRow(iteration);
      } else {
        return null;
      }
    }
  };


  /**
   * adds an extra property to the passed element that is gonna be used for
   * the ordering algorithm
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Object]} item
   * @param  {[Number]} index
   * @return {[Object]} item
   */

  TileArrangement.prototype._tagElement = function(item, index) {
    return item._strengthPoints = typeof index === "number" ? index : 'wildcard';
  };


  /**
   * get an element and delete it from the selected collection
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[String]} index
   * @return {[Object]} item
   */

  TileArrangement.prototype._grabElement = function(index) {
    var item;
    item = this.population[index][0];
    this.population[index] = this.population[index].slice(1);
    return item;
  };


  /**
   * check if an element is a wildcard
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Object]}  element
   * @return {Boolean}
   */

  TileArrangement.prototype._isWildCard = function(element) {
    if (typeof element._strengthPoints === "number") {
      return false;
    } else {
      return true;
    }
  };


  /**
   * finds a random number (between 2 and 5) of wildcards per row
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Number]} iteration = 0
   * @return {[Number]}
   */

  TileArrangement.prototype._findRandomWildcardLimitPerRow = function(iteration) {
    var iterationLimit, maxWilcardPerRow, minWilcardPerRow, randomWilcardLimit, wildcardPopulationLength;
    if (iteration == null) {
      iteration = 0;
    }
    wildcardPopulationLength = this.population['wildcard'].length + 1;
    if (wildcardPopulationLength <= 3) {
      return wildcardPopulationLength;
    }
    minWilcardPerRow = 2;
    maxWilcardPerRow = 5;
    randomWilcardLimit = _.random(minWilcardPerRow, maxWilcardPerRow);
    iterationLimit = 20;
    if (iteration < 20) {
      if (wildcardPopulationLength > randomWilcardLimit && wildcardPopulationLength - randomWilcardLimit >= 2 && iteration < 10) {
        return randomWilcardLimit;
      } else {
        iteration++;
        return this._findRandomWildcardLimitPerRow(iteration);
      }
    } else {
      return minWilcardPerRow;
    }
  };


  /**
   * Determines the maximun amount of wildcards per row
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Object]}  element
   * @param  {Boolean} isWildCard
   * @return {[Number]}
   */

  TileArrangement.prototype._maxWildcardsPerRow = function(element, isWildCard) {
    var randomWilcardLimit;
    if (isWildCard) {
      randomWilcardLimit = this._findRandomWildcardLimitPerRow();
      return randomWilcardLimit;
    } else {
      return this.opt.wildcardLimitPerRow;
    }
  };


  /**
   * fills the remaining row space based on the passed element
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @param  {[Object]} element
   * @return {[Array]}
   */

  TileArrangement.prototype._completeRowSpace = function(element) {
    var isWildCard, orderedRow, wildcardLimitPerRow, _spaceToFill;
    isWildCard = this._isWildCard(element);
    wildcardLimitPerRow = this._maxWildcardsPerRow(element, isWildCard);
    if (typeof element._strengthPoints === "number") {
      _spaceToFill = this.opt.totalPointsPerRow - element._strengthPoints;
    } else {
      _spaceToFill = 'wildcard';
    }
    this.groupingObj.setCurrentRowValues({
      population: this.population,
      remainingPoints: _spaceToFill,
      row: [element],
      wildcardLimitPerRow: wildcardLimitPerRow
    });
    orderedRow = this.grouper.getOrderedRow();
    return orderedRow;
  };

  return TileArrangement;

})();

module.exports = window.ngk.plugins.TileArrangement;


},{"./grouper":3,"./groupingrules__factory":4,"./layout__factory":6,"./layouter":8}],10:[function(require,module,exports){
var Utils;

Utils = (function() {
  function Utils() {}


  /**
   * This method uses a variation of the javascript version of
   * http://detectmobilebrowsers.com/
   * @author Francisco Ramini <francisco.ramini at globant.com>
   * @return {Boolean} [description]
   */

  Utils.isMobile = function() {
    var check;
    check = false;
    (function(a) {
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
        check = true;
      }
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
  };

  return Utils;

})();

module.exports = Utils;


},{}]},{},[9,1,2])