/**
 * The purpose of this class is to be the client for our strategy
 * pattern
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var Layouter;

  Layouter = (function() {
    function Layouter(strategy) {
      _.bindAll(this, 'getLayout');
      this.strategy = strategy;
    }

    Layouter.prototype.getLayout = function() {
      return this.strategy.buildLayout();
    };

    return Layouter;

  })();

  module.exports = Layouter;

}).call(this);
