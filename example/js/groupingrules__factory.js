/**
 * Factory class for fillingrules objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var GroupingRulesFactory;

  GroupingRulesFactory = (function() {
    var GroupingRulesFactorySingleton, instance;

    function GroupingRulesFactory() {}

    instance = null;

    GroupingRulesFactorySingleton = (function() {
      function GroupingRulesFactorySingleton() {
        _.bindAll(this, 'setStrategies', 'createGroupingRules');
        this.strategy = null;
      }

      /**
       * public method to set the group of strategies that are gonna be used by the library
       * @author Francisco Ramini <francisco.ramini at globant.com>
       * @param  {[GroupingRuleStrategy]} strategy
       * @param  {[String]} platform
      */


      GroupingRulesFactorySingleton.prototype.setStrategies = function(strategy) {
        this.strategy = strategy;
        return this;
      };

      /**
       * [createGroupingRules description]
       * @author Francisco Ramini <francisco.ramini at globant.com>
       * @param  {[Number]} firstElementPoints
       * @param  {[Array]} row
       * @return {[GroupingRuleStrategy]}
      */


      GroupingRulesFactorySingleton.prototype.createGroupingRules = function() {
        if (this.strategy != null) {
          return new this.strategy();
        } else {
          throw new Error("You need to set a grouping strategy before you can use it.");
        }
      };

      return GroupingRulesFactorySingleton;

    })();

    GroupingRulesFactory.getInstance = function() {
      return instance != null ? instance : instance = new GroupingRulesFactorySingleton();
    };

    return GroupingRulesFactory;

  })();

  module.exports = GroupingRulesFactory;

}).call(this);
