/**
 * Factory class for layout objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var LayoutStrategyFactory;

  LayoutStrategyFactory = (function() {
    var LayoutStrategyFactorySingleton, instance;

    function LayoutStrategyFactory() {}

    instance = null;

    LayoutStrategyFactorySingleton = (function() {
      function LayoutStrategyFactorySingleton() {
        _.bindAll(this, 'setStrategies', 'createLayout');
        this.strategies = {};
      }

      /**
       * public method to set the group of strategies that are gonna be used by the library
       * @author Francisco Ramini <francisco.ramini at globant.com>
       * @param  {[Array]} strategies
      */


      LayoutStrategyFactorySingleton.prototype.setStrategies = function(strategies) {
        var _this = this;
        if (_.isArray(strategies)) {
          _.each(strategies, function(strategyObj, index) {
            return _this.strategies[strategyObj.points] = strategyObj.layoutClass;
          });
        } else {
          throw new Error("The param passed to [LayoutStrategyFactory].setStrategies() must be an array");
        }
        return this;
      };

      /**
       * [createLayout description]
       * @author Francisco Ramini <francisco.ramini at globant.com>
       * @param  {[Number]} firstElementPoints
       * @param  {[Array]} row
       * @return {[LayoutStrategy]}
      */


      LayoutStrategyFactorySingleton.prototype.createLayout = function(firstElementPoints, row) {
        var layoutStrategy;
        if (this.strategies[firstElementPoints]) {
          layoutStrategy = new this.strategies[firstElementPoints]();
          layoutStrategy.init(row);
          return layoutStrategy;
        } else {
          throw new Error("The layout with points: " + firstElementPoints + " is not defined.");
        }
      };

      return LayoutStrategyFactorySingleton;

    })();

    LayoutStrategyFactory.getInstance = function() {
      return instance != null ? instance : instance = new LayoutStrategyFactorySingleton();
    };

    return LayoutStrategyFactory;

  })();

  module.exports = LayoutStrategyFactory;

}).call(this);
