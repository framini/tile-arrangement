/**
 * Concrete instance of LayoutStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var LayoutStrategy, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  LayoutStrategy = require('./layout__strategy');

  window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy = (function(_super) {
    __extends(ConcreteLayoutStrategy, _super);

    function ConcreteLayoutStrategy() {
      _ref = ConcreteLayoutStrategy.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    return ConcreteLayoutStrategy;

  })(LayoutStrategy);

  module.exports = window.ngk.utils.ConcreteLayoutStrategy;

}).call(this);
