/**
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
*/


(function() {
  var LayoutStrategy;

  LayoutStrategy = (function() {
    function LayoutStrategy(row) {
      _.bindAll(this, 'getLayout', '_buildLayoutRules', 'init', 'setRules', 'buildLayout');
    }

    LayoutStrategy.prototype.init = function(row) {
      this._mobileRules = {};
      this._desktopRules = {};
      this._rowDesktopRules = {};
      this._rowMobileRules = {};
      this.currentRules = [];
      this.currentRowRules = [];
      this._rowElements = row;
      this._generalTileOptions = {
        classes: "tile"
      };
      return this.rowLength = this._rowElements.length;
    };

    /**
     * this method is used by our client class (strategy pattern) to call
     * all the needed method to create the layoyt
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    */


    LayoutStrategy.prototype.buildLayout = function() {
      this.setRules();
      this.currentRules = this._setLayout(this._rowElements, this.currentRules);
      return this.getLayout();
    };

    /**
     * set the rules for the corresponding platform (mobile or desktop)
     * @author Francisco Ramini <francisco.ramini at globant.com>
    */


    LayoutStrategy.prototype.setRules = function() {
      if (true) {
        this.setDesktopRules();
      }
      this._buildLayoutRules();
      return this;
    };

    /**
     * abstract method that will be overriden by the subclasses and
     * will allow us to set all the filling rules for mobile
     * @author Francisco Ramini <francisco.ramini at globant.com>
    */


    LayoutStrategy.prototype.setMobileRules = function() {};

    /**
     * abstract method that will be overriden by the subclasses and
     * will allow us to set all the filling rules for desktop
     * @author Francisco Ramini <francisco.ramini at globant.com>
    */


    LayoutStrategy.prototype.setDesktopRules = function() {};

    /**
     * This method returns an object containing the rules for each tile
     * and another object containing the rules to be applied in the row
     * container
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    */


    LayoutStrategy.prototype.getLayout = function() {
      return {
        tileRules: this.currentRules,
        rowRules: this._rowDesktopRules
      };
    };

    /**
     * method to alter the object representing a tile with the properties that
     * will be used to generate the layout (used in the related template files)
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[type]} iteration = 0
     * @return {[type]}
    */


    LayoutStrategy.prototype._buildLayoutRules = function(iteration) {
      var baseObj;
      if (iteration == null) {
        iteration = 0;
      }
      baseObj = {
        _domArrangement: {}
      };
      baseObj._domArrangement = _.extend(baseObj._domArrangement, _.isArray(this._desktopRules) ? this._desktopRules[iteration] : this._desktopRules);
      this.currentRules.push(baseObj);
      if (iteration < this.rowLength - 1) {
        iteration++;
        return this._buildLayoutRules(iteration);
      }
    };

    /**
     * this method will be in charge of adding the different layout styles (i.e number of columns,
     * size element, etc) to each tile
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} row
     * @param  {[Array/Object]} tileProps
     * @param  {[Number]} iteration = 0
     * @return {[Array]} rows
    */


    LayoutStrategy.prototype._setLayout = function(row, tileProps, iteration) {
      if (iteration == null) {
        iteration = 0;
      }
      _.extend(row[iteration], _.isArray(tileProps) ? tileProps[iteration] : tileProps, {
        _generalOptions: this._generalTileOptions
      });
      if (iteration < row.length - 1) {
        iteration++;
        return this._setLayout(row, tileProps, iteration);
      } else {
        return row;
      }
    };

    return LayoutStrategy;

  })();

  module.exports = LayoutStrategy;

}).call(this);
