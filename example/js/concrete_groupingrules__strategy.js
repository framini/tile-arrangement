/**
 * Concrete instance of GroupingRulesStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
*/


(function() {
  var GroupingRulesStrategy, _ref,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  GroupingRulesStrategy = require('./groupingrules__strategy');

  window.ngk.plugins.TileArrangement.ConcreteGroupingRulesStrategy = (function(_super) {
    __extends(ConcreteGroupingRulesStrategy, _super);

    function ConcreteGroupingRulesStrategy() {
      _ref = ConcreteGroupingRulesStrategy.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    return ConcreteGroupingRulesStrategy;

  })(GroupingRulesStrategy);

  module.exports = window.ngk.utils.ConcreteGroupingRulesStrategy;

}).call(this);
