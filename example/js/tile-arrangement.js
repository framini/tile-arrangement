/**
 * The purpose of this class is to arrange a group of tiles
 * in rows according to a set of styling rules defined by the
 * design team, that allows each row to have its own layout.
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var Grouper, GroupingRulesFactory, LayoutStrategyFactory, Layouter;

  Grouper = require('./grouper');

  LayoutStrategyFactory = require('./layout__factory');

  GroupingRulesFactory = require('./groupingrules__factory');

  Layouter = require('./layouter');

  /**
   * to avoid the consumer of this library to use the it as a module
   * we'll expose the main class as part of the window obj
  */


  window.ngk.plugins.TileArrangement = (function() {
    /**
     * constructor method
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} elems   = {}
     * @param  {[Object]} options = {}
    */

    function TileArrangement(elems, options) {
      if (elems == null) {
        elems = {};
      }
      if (options == null) {
        options = {};
      }
      _.bindAll(this, 'arrange', '_arrangeRow', '_grabFirstElementRow', '_completeRowSpace', '_grabElement', '_maxWildcardsPerRow', '_doDomArrangement', '_addDomFillingOptions', '_findRandomWildcardLimitPerRow', '_init', '_groupPopulation', '_createCateogoryPointsArray', 'setNewCollection');
      this.opt = {};
      this._init(elems, options);
    }

    TileArrangement.prototype._init = function(elems, options) {
      var defaults;
      defaults = {
        wildcardLimitPerRow: 3,
        stepPoints: 25,
        totalPointsPerRow: 100
      };
      _.extend(this.opt, defaults, options);
      this.elems = elems;
      this.elemsCpy = this.elems;
      this.rows = [];
      this.layouts = {};
      this.layoutFactory = LayoutStrategyFactory.getInstance();
      this.layoutFactory.setStrategies(this.opt.layoutStrategies);
      this.groupingFactory = GroupingRulesFactory.getInstance();
      if (!!this.opt.groupingStrategy) {
        this.groupingFactory.setStrategies(this.opt.groupingStrategy);
      } else {
        throw new Error("The option groupingStrategy is mandatory");
      }
      this.groupingObj = this.groupingFactory.createGroupingRules();
      this.grouper = Grouper.getInstance(this.groupingObj);
      this.population = {};
      if (!!this.opt.tileTypes.length) {
        this._createCateogoryPointsArray();
        return this._groupPopulation();
      } else {
        throw new Error("The option tileTypes is mandatory");
      }
    };

    /**
     * creates an ordered array with the elements passed in the tileTypes
     * options
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Array]}
    */


    TileArrangement.prototype._createCateogoryPointsArray = function() {
      var _this = this;
      this.cateogoryPointsArray = [];
      _.each(this.opt.tileTypes, function(element, index, list) {
        return _this.cateogoryPointsArray.push(element.points);
      });
      this.cateogoryPointsArray.push('wildcard');
      return this.cateogoryPointsArray.sort(function(a, b) {
        return b - a;
      });
    };

    /**
     * set a new collection after the library has been initialized
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} collection
     * @param  {[Object]} options    = {}
    */


    TileArrangement.prototype.setNewCollection = function(collection, options) {
      if (options == null) {
        options = {};
      }
      return this._init(collection, options);
    };

    /**
     * Recursive method that is used to initialize the population object that
     * is used to hold the member of each of the passed categories
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} iteration = 0
    */


    TileArrangement.prototype._groupPopulation = function(iteration) {
      var groupSettings;
      if (iteration == null) {
        iteration = 0;
      }
      groupSettings = this.opt.tileTypes[iteration];
      this.population[groupSettings.points] = _.where(this.elemsCpy, {
        type: groupSettings.category
      });
      this.elemsCpy = _.difference(this.elemsCpy, this.population[groupSettings.points]);
      iteration++;
      if (!!this.opt.tileTypes[iteration]) {
        return this._groupPopulation(iteration);
      } else {
        return this.population['wildcard'] = this.elemsCpy;
      }
    };

    /**
     * this method will be in charge of ordering a group of tiles and will
     * be the "public" method that needs to be called whenever we need to
     * to order a group of tiles
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]} arrays
    */


    TileArrangement.prototype.arrange = function() {
      var _rows;
      _rows = [];
      _rows = this._arrangeRow();
      _rows = this._doDomArrangement(_rows);
      this.rows = _rows;
      return this.rows;
    };

    /**
     * this method is in charge of adding the necesary options on each object (tile)
     * to correctly display the row in the browser
     * The returning array return an object with the row and the props that needs to be applied
     * to the container of the row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} tilesCollection
     * @return {[Object]} rows
    */


    TileArrangement.prototype._doDomArrangement = function(tilesCollection) {
      var rowProps, rows,
        _this = this;
      rowProps = [];
      rows = [];
      _.each(tilesCollection, function(row, rowNum) {
        var result;
        result = _this._addDomFillingOptions(row);
        rowProps.push(result.rowProps);
        return rows.push(result.row);
      });
      return {
        rowProps: rowProps,
        rows: rows
      };
    };

    /**
     * this method will be in charge of adding the corresponding
     * properties (css options mostly) to the elems that will allow them to be displayed
     * correctly in the browser
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} row
    */


    TileArrangement.prototype._addDomFillingOptions = function(row) {
      var firstElement, firstElementPoints, layoutObj, layoutRules, layouter, rowLevelProps, rowProps, tileProps;
      rowLevelProps = [];
      firstElement = row[0];
      firstElementPoints = firstElement._strengthPoints;
      layoutObj = this.layoutFactory.createLayout(firstElementPoints, row);
      layouter = new Layouter(layoutObj);
      layoutRules = layouter.getLayout();
      tileProps = layoutRules.tileRules;
      rowProps = layoutRules.rowRules;
      rowLevelProps.push(rowProps);
      return {
        row: tileProps,
        rowProps: rowLevelProps
      };
    };

    /**
     * recursive method to iterate over all of the collections an arrange them
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Array]} rows
    */


    TileArrangement.prototype._arrangeRow = function(rows, iteration) {
      var firstElement, row, _rows;
      if (rows == null) {
        rows = [];
      }
      if (iteration == null) {
        iteration = 0;
      }
      _rows = rows || [];
      firstElement = this._grabFirstElementRow();
      if (firstElement) {
        row = this._completeRowSpace(firstElement);
        _rows.push(row);
        iteration++;
        return this._arrangeRow(_rows, iteration);
      } else {
        return _rows;
      }
    };

    /**
     * grab (get and erase an element from a collection) the first element for the row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    */


    TileArrangement.prototype._grabFirstElementRow = function(iteration) {
      var item;
      if (iteration == null) {
        iteration = 0;
      }
      if (!!this.population[this.cateogoryPointsArray[iteration]].length) {
        item = this._grabElement(this.cateogoryPointsArray[iteration]);
        this._tagElement(item, this.cateogoryPointsArray[iteration]);
        return item;
      } else {
        iteration++;
        if (iteration < this.cateogoryPointsArray.length) {
          return this._grabFirstElementRow(iteration);
        } else {
          return null;
        }
      }
    };

    /**
     * adds an extra property to the passed element that is gonna be used for
     * the ordering algorithm
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]} item
     * @param  {[Number]} index
     * @return {[Object]} item
    */


    TileArrangement.prototype._tagElement = function(item, index) {
      return item._strengthPoints = typeof index === "number" ? index : 'wildcard';
    };

    /**
     * get an element and delete it from the selected collection
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[String]} index
     * @return {[Object]} item
    */


    TileArrangement.prototype._grabElement = function(index) {
      var item;
      item = this.population[index][0];
      this.population[index] = this.population[index].slice(1);
      return item;
    };

    /**
     * check if an element is a wildcard
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]}  element
     * @return {Boolean}
    */


    TileArrangement.prototype._isWildCard = function(element) {
      if (typeof element._strengthPoints === "number") {
        return false;
      } else {
        return true;
      }
    };

    /**
     * finds a random number (between 2 and 5) of wildcards per row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} iteration = 0
     * @return {[Number]}
    */


    TileArrangement.prototype._findRandomWildcardLimitPerRow = function(iteration) {
      var iterationLimit, maxWilcardPerRow, minWilcardPerRow, randomWilcardLimit, wildcardPopulationLength;
      if (iteration == null) {
        iteration = 0;
      }
      wildcardPopulationLength = this.population['wildcard'].length + 1;
      if (wildcardPopulationLength <= 3) {
        return wildcardPopulationLength;
      }
      minWilcardPerRow = 2;
      maxWilcardPerRow = 5;
      randomWilcardLimit = _.random(minWilcardPerRow, maxWilcardPerRow);
      iterationLimit = 20;
      if (iteration < 20) {
        if (wildcardPopulationLength > randomWilcardLimit && wildcardPopulationLength - randomWilcardLimit >= 2 && iteration < 10) {
          return randomWilcardLimit;
        } else {
          iteration++;
          return this._findRandomWildcardLimitPerRow(iteration);
        }
      } else {
        return minWilcardPerRow;
      }
    };

    /**
     * Determines the maximun amount of wildcards per row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]}  element
     * @param  {Boolean} isWildCard
     * @return {[Number]}
    */


    TileArrangement.prototype._maxWildcardsPerRow = function(element, isWildCard) {
      var randomWilcardLimit;
      if (isWildCard) {
        randomWilcardLimit = this._findRandomWildcardLimitPerRow();
        return randomWilcardLimit;
      } else {
        return this.opt.wildcardLimitPerRow;
      }
    };

    /**
     * fills the remaining row space based on the passed element
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]} element
     * @return {[Array]}
    */


    TileArrangement.prototype._completeRowSpace = function(element) {
      var isWildCard, orderedRow, wildcardLimitPerRow, _spaceToFill;
      isWildCard = this._isWildCard(element);
      wildcardLimitPerRow = this._maxWildcardsPerRow(element, isWildCard);
      if (typeof element._strengthPoints === "number") {
        _spaceToFill = this.opt.totalPointsPerRow - element._strengthPoints;
      } else {
        _spaceToFill = 'wildcard';
      }
      this.groupingObj.setCurrentRowValues({
        population: this.population,
        remainingPoints: _spaceToFill,
        row: [element],
        wildcardLimitPerRow: wildcardLimitPerRow
      });
      orderedRow = this.grouper.getOrderedRow();
      return orderedRow;
    };

    return TileArrangement;

  })();

  module.exports = window.ngk.plugins.TileArrangement;

}).call(this);
