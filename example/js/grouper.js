/**
 * The purpose of this class is to be the client for our grouping strategy
 * pattern.
 *
 * This is gonna be a singleton since one grouping strategy can be applied
 * at a time
 * @author Francisco Ramini <francisco.ramini at globant.com>
*/


(function() {
  var Grouper;

  Grouper = (function() {
    var instance;

    function Grouper() {}

    instance = null;

    Grouper = (function() {
      function Grouper(strategy) {
        _.bindAll(this, 'getOrderedRow', 'setStrategy');
        this.strategy = strategy;
      }

      Grouper.prototype.getOrderedRow = function() {
        return this.strategy.buildGroupingRules();
      };

      Grouper.prototype.setStrategy = function(strategy) {
        return this.strategy = strategy;
      };

      return Grouper;

    })();

    Grouper.getInstance = function(strategy) {
      if (instance != null) {
        instance.setStrategy(strategy);
        return instance;
      } else {
        return instance = new Grouper(strategy);
      }
    };

    return Grouper;

  })();

  module.exports = Grouper;

}).call(this);
