/**
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
*/


(function() {
  var GroupingRulesStrategy;

  GroupingRulesStrategy = (function() {
    function GroupingRulesStrategy() {
      _.bindAll(this, 'buildGroupingRules', 'setRules', 'setDesktopRules', 'setMobileRules', '_grabElement', 'setCurrentRowValues');
    }

    GroupingRulesStrategy.prototype.setCurrentRowValues = function(options) {
      this.population = options.population;
      this.remainingPoints = options.remainingPoints;
      this.row = options.row;
      this.wildcardLimitPerRow = options.wildcardLimitPerRow;
      return this.orderedRow = [];
    };

    /**
     * This method is gonna be used by the client (in the strategy pattern) to
     * run the primary algorithms that applies the ordering logic
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[type]}
    */


    GroupingRulesStrategy.prototype.buildGroupingRules = function() {
      return this.setRules();
    };

    GroupingRulesStrategy.prototype.setRules = function() {
      if (true) {
        return this.setDesktopRules();
      }
    };

    /**
     * Abstract method that is gonna be implemented by the subclass
     * @author Francisco Ramini <francisco.ramini at globant.com>
    */


    GroupingRulesStrategy.prototype.setDesktopRules = function() {};

    /**
     * Abstract method that is gonna be implemented by the subclass
     * @author Francisco Ramini <francisco.ramini at globant.com>
    */


    GroupingRulesStrategy.prototype.setMobileRules = function() {};

    /**
     * get an element and delete it from the selected collection
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[String]} index
     * @return {[Object]} item
    */


    GroupingRulesStrategy.prototype._grabElement = function(index) {
      var item;
      item = this.population[index][0];
      this.population[index] = this.population[index].slice(1);
      return item;
    };

    return GroupingRulesStrategy;

  })();

  module.exports = GroupingRulesStrategy;

}).call(this);
