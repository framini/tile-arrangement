###*
 * Challenge Strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class window.ngk.utils.ChallengeLayoutStrategy extends window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy

    # this method will allow us to set all the filling rules for mobile
    setMobileRules: () ->

    # this method will allow us to set all the filling rules for desktop
    setDesktopRules: () ->
        # this case could be 2 challenges or 1 challenge + 1wc
        if @rowLength == 2

            if @_rowElements[1].type != "challenge" # TODO: Cambiar el filtro por "type". En un futuro vamos a tener un nuevo prop para esto

                @_desktopRules = [
                        styles :
                            "margin-left"  : "-500px"
                            "float"        : "left"
                            "height"       : "100%"
                            "width"        : "500px"
                    ,
                        styles :
                            "width"        : "100%"
                            "height"       : "100%"
                ]

                @_rowDesktopRules =
                    "padding-left"  : "500px"
                    "height"        : "29.375rem"

            else
                # TODO: check this case with real challenges
                @_desktopRules =
                    styles :
                        "width"  : "50%"
                        "height" : "100%"

                @_rowDesktopRules =
                    "height"    : "29.375rem"


        # 1 50pts + 25pts + 25pts
        else if @rowLength == 3

            @_desktopRules = [
                    styles :
                        "width"  : "50%"
                        "height" : "29.375rem" #
                ,
                    styles :
                        "width"  : "25%"
                        "height" : "29.375rem"
                ,
                    styles :
                        "width"  : "25%"
                        "height" : "29.375rem"
            ]

            @_rowDesktopRules =
                "height"    : "29.375rem"

        # 1 50pts +25pts + 2 wildcard
        else if @rowLength > 3

            @_desktopRules = [
                    styles :
                        "width"  : "50%"
                        "height" : "29.375rem" #
                ,
                    styles :
                        "width"  : "25%"
                        "height" : "29.375rem"
                ,
                    styles :
                        "width"  : "25%"
                        "height" : "50%"
                ,
                    styles :
                        "width"  : "25%"
                        "height" : "50%"
            ]

            @_rowDesktopRules =
                "height"    : "29.375rem"

