$ ->

    # demo helpers
    ##############################
    Handlebars.registerHelper("randomImage", () ->
        return "http://lorempixel.com/800/600/?" + Math.ceil( Math.random() * 1000 )
    )

    TilesCollection = Backbone.Collection.extend({
    })

    RowCollection = Backbone.Collection.extend({
    })

    RowContainer = Backbone.View.extend
        initialize:() ->
            @template = JST['rowcontainer']
            @subViews = []

        render: () ->
            renderedContent = @template @model
            # @$el.html(renderedContent)
            @setElement(renderedContent)


            classes = []
            _.each(@subViews, (row, index) =>
                classes.push "#" + row.id
                @$el.append row
            )

            $('.bg-container').append( @el )

            _.each(classes, (element, index) ->
                $(element).imagesLoaded().always( ( instance ) ->
                    $(element).addClass('animated bounceIn')
                )
            )

            return @

        setSubViews : (subView) ->
            @subViews.push subView

    TileView = Backbone.View.extend

        template: JST['tile']

        initialize:() ->

            _.bindAll this, 'render',
                            'setColumnProps'
        ,
        render: () ->
            renderedContent = @template( @model )

            @setElement( renderedContent )

            return @
        ,
        setColumnProps : (columnProps) ->
          @columnProps = columnProps

    tiles = [{"name":"metadata","type":"poll","color":"F43022","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">Scorpion<\/p>\n","width":"2","height":"2","availability":"basic","image":"/content/dam/kids/photos/tiles/Scorpion.jpg","altText":"Scorpions can quickly grab an insect with their pincers and whip their telson, the poisonous tip of their segmented tail forward and sting their prey. ","credit":"Photograph by Otto Hahn, Picture Press/Photolibrary","caption":"Scorpions can quickly grab an insect with their pincers and whip their telson, the poisonous tip of their segmented tail forward and sting their prey. ","subtitle":"Poisonous Tail"},{"name":"metadata","type":"challenge","color":"3FAE7B","title":"<p>Anaconda<\/p>\n","width":"3","height":"2","availability":"basic","image":"/content/dam/kids/photos/tiles/Anaconda.jpg","altText":"Anacondas swim in the water","credit":"Photograph by Ed George","caption":"The 71BA01 anaconda can stay underwater for as long as ten minutes without coming to the surface to breathe.","subtitle":"Squeeze its prey"},{"name":"jcr:content","type":"challenge","color":"89F895","title":"<p>Which one is faster?<\/p>\n","width":"2","height":"2","availability":"basic","animal1Name":"Coyote","animal1Href":"/content/kids/en_US/animals-landing-page/coyote","animal1Image":"/content/dam/kids/photos/animals/Coyote/coyote_1.jpg","animal2Name":"Elephant","animal2Href":"/content/kids/en_US/animals-landing-page/elephant","animal2Image":"/content/dam/kids/photos/animals/Elephant/elephant_1.jpg","makeYourChoice":"Make your choice","learnMore":"Learn more","guid":"challenge-4"},{"name":"jcr:content","type":"did-you-know","color":"00AAFF","width":"1","height":"2","availability":"basic","moreFacts":"More facts","title":"Did you know?","landingPageURL":"/content/kids/en_US/fun-facts-landing","showMoreFuctsButton":true,"description":"This is the abstract of the FFact"},{"name":"jcr:content","type":"poll","color":"ff4354","width":"1","height":"2","availability":"basic","moreFacts":"More facts","title":"Did you know?","landingPageURL":"/content/kids/en_US/fun-facts-landing","showMoreFuctsButton":true,"description":"This is the abstract of the FFact"},{"name":"jcr:content","type":"poll","color":"2345FF","width":"1","height":"2","availability":"basic","moreFacts":"More facts","title":"Did you know?","landingPageURL":"/content/kids/en_US/fun-facts-landing","showMoreFuctsButton":true,"description":"This is the abstract of the FFact"},{"name":"jcr:content","type":"poll","color":"543f43","width":"1","height":"2","availability":"basic","moreFacts":"More facts","title":"Did you know?","landingPageURL":"/content/kids/en_US/fun-facts-landing","showMoreFuctsButton":true,"description":"This is the abstract of the FFact"},{"name":"jcr:content","type":"poll","color":"946DB0","title":"<p>Polar Bear<\/p>\n","width":"2","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/polar_bear.html","image":"/content/dam/kids/photos/animals/polar_bear/polar_bear_1.jpg","subtitle":"These bears have been known to swim 100 miles (161 kilometers) at a stretch"},{"name":"jcr:content","type":"animalProfile","color":"FCB913","title":"<p>Chimp<\/p>\n","width":"1","height":"1","availability":"basic","link":"/content/kids/en_US/animals-landing-page/chimp.html","image":"/content/dam/kids/photos/animals/chimp/chimp_1.jpg","subtitle":"Chimpanzee"},{"name":"jcr:content","type":"animal","color":"C0D731","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">How would you like to see a tiger?<\/p>\n","width":"1","height":"2","availability":"basic","pollImage":"/content/dam/kids/photos/Polls/Tiger/tiger1.jpg","resultImage":"/content/dam/kids/photos/Polls/Tiger/tiger2.jpg","options":[{"option":"On safari"},{"option":"In the zoo"},{"option":"In NG Kids magazine"},{"option":"In my dreams"}],"guid":"poll-12"},{"name":"jcr:content","type":"photo","color":"F43022","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">Triceratops<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/triceratops.html","subtitle":"Three Horns"},{"name":"jcr:content","type":"photo","color":"F7941D","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">GAL�PAGOS TORTOISE<\/p>\n","width":"1","height":"1","availability":"basic","family":"reptiles","link":"/content/kids/en_US/animals-landing-page/galapagos-tortoise.html","subtitle":"SLOW-MOVING REPTILE"},{"name":"jcr:content","type":"poll","color":"F43022","title":"<p>BOA CONSTRICTOR<\/p>\n","width":"1","height":"1","availability":"basic","family":"reptiles","link":"/content/kids/en_US/animals-landing-page/boa-constrictor.html","subtitle":"SQUEEZING HUNTER"},{"name":"jcr:content","type":"poll","color":"40CDBB","title":"<p>LEATHERBACK SEA TURTLES<\/p>\n","width":"1","height":"1","availability":"basic","family":"reptiles","link":"/content/kids/en_US/animals-landing-page/leatherback-sea-turtle.html","subtitle":"SHELLS LIKE LEATHER"},{"name":"jcr:content","type":"poll","color":"71BA01","title":"<p>GIANT SQUID<\/p>\n","width":"1","height":"1","availability":"basic","family":"invertebrates","link":"/content/kids/en_US/animals-landing-page/giant-squid.html","subtitle":"BEACH BALL EYES!"},{"name":"jcr:content","type":"animalProfile","color":"3FAE7B","title":"<p>BELUGA WHALE<\/p>\n","width":"1","height":"1","availability":"basic","link":"/content/kids/en_US/animals-landing-page/beluga-whale.html","subtitle":"ONE OF THE SMALLER WHALES"},{"name":"jcr:content","type":"challenge","color":"946DB0","title":"<p>GILA MONSTER<\/p>\n","width":"1","height":"1","availability":"basic","family":"reptiles","link":"/content/kids/en_US/animals-landing-page/gila-monster.html","subtitle":"EATS ONE MEAL A YEAR!"},{"name":"jcr:content","type":"challenge","color":"AE3F90","title":"<p>LOGGERHEAD SEA TURTLE<\/p>\n","width":"1","height":"1","availability":"basic","family":"reptiles","link":"/content/kids/en_US/animals-landing-page/loggerhead-sea-turtle.html","subtitle":"HEAD LIKE A LOG!"},{"name":"jcr:content","type":"animalProfile","color":"AE3F90","title":"<p>CHIPMUNK<\/p>\n","width":"1","height":"1","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/chipmunk.html","subtitle":"CHIRP WHEN THREATENED "},{"name":"jcr:content","type":"animalProfile","color":"F7941D","title":"<p>COYOTE<\/p>\n","width":"1","height":"1","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/coyote.html","subtitle":"CLEVER AND ADAPTIVE "},{"name":"jcr:content","type":"animalProfile","color":"35C2F7","title":"<p>Ankylosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/ankylosaurus.html","subtitle":"Serious Defenses"},{"name":"jcr:content","type":"animalProfile","color":"008BC0","title":"<p>Brachiosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/brachiosaurus.html","subtitle":"How Big Was It?"},{"name":"jcr:content","type":"animalProfile","color":"35C2F7","title":"<p>Apatosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/apatosaurus.html","subtitle":"Treetop Eater"},{"name":"jcr:content","type":"animalProfile","color":"946DB0","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">Archaeopteryx<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/archaeopteryx.html","subtitle":"A Real Early Bird"},{"name":"jcr:content","type":"animalProfile","color":"FCB913","title":"<p>Dracorex<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/dracorex.html","subtitle":"Dragon King"},{"name":"jcr:content","type":"animalProfile","color":"C0D731","title":"<p>Iguanodon<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/iguanodon.html","subtitle":"First Identified Dino"},{"name":"jcr:content","type":"animalProfile","color":"008BC0","title":"<p style=\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 12px;\">Dilophosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/dilophosaurus.html","subtitle":"Double-Crested Carnivore"},{"name":"jcr:content","type":"animalProfile","color":"FCB913","title":"<p>Allosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/allosaurus.html","subtitle":"Giant Meat-Eater"},{"name":"jcr:content","type":"animalProfile","color":"F37022","title":"<p>Beaver&nbsp;&nbsp;&nbsp;&nbsp;<\/p>\n","width":"1","height":"1","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/beaver.html","subtitle":"Some of the world's largest rodents"},{"name":"jcr:content","type":"animalProfile","color":"40CDBB","title":"<p>Spinosaurus<\/p>\n","width":"1","height":"1","availability":"basic","family":"dinosaurs-prehistoric","link":"/content/kids/en_US/animals-landing-page/spinosaurus.html","subtitle":"Tall Back Spines"}, {"name":"jcr:content","type":"animalProfile","color":"008BC0","title":"<p><b>Crazzy<\/b> Chimps!<\/p>\n","width":"1","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/CHIMP.html","image":"/content/dam/kids/chimp/Chimp_tile.jpg"},{"name":"jcr:content","type":"animalProfile","color":"F37022","title":"<p>Happy Gigant<\/p>\n","width":"1","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/elephant.html","image":"/content/dam/kids/elephant/elephant_tile.jpg"},{"name":"jcr:content","type":"animalProfile","color":"3FAE7B","title":"<p>American Bison<\/p>\n","width":"2","height":"1","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/bison.html","image":"/content/dam/kids/bison/bison tile.jpg"},{"name":"jcr:content","type":"animalProfile","color":"F43022","title":"<p>Black Rhino!!<\/p>\n","width":"3","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/rhinoceroses.html","image":"/content/dam/kids/rhino/black rhino & calf.jpg","subtitle":"Citizens of Kenya"},{"name":"jcr:content","type":"animalProfile","color":"AE3F90","title":"<p><b>King<\/b> of the Jungle<\/p>\n","width":"2","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/lion.html","image":"/content/dam/kids/lion/lion_tile.jpg"},{"name":"jcr:content","type":"animalProfile","color":"71BA01","title":"<p>Atlantic Puffins<\/p>\n","width":"1","height":"1","availability":"basic","family":"birds","link":"/content/kids/en_US/animals-landing-page/puffins.html","image":"/content/dam/kids/puffins/Puffinf.jpg"},{"name":"jcr:content","type":"animalProfile","color":"946DB0","title":"<p>Pandas in China<\/p>\n","width":"1","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/panda.html","image":"/content/dam/kids/panda/Sweet-Panda-pandas-12538469-1600-1200.jpg","subtitle":"Bamboo Fans"},{"name":"jcr:content","type":"animalProfile","color":"C0D731","title":"<p><b>Powerful<\/b> Bear asfafa!<\/p>\n","width":"2","height":"2","availability":"basic","family":"mammals","link":"/content/kids/en_US/animals-landing-page/polar_bear.html","image":"/content/dam/kids/polar_bear/tile.jpg","subtitle":"Rulers in the North Pole"}]

    tilesCollection = new TilesCollection()

    tilesCollection.reset(tiles)

    ## finish demo helpers
    ###############################

    ###*
     * Module feed usage
    ###

    # Initialize the moduleFeed instance with:
    # - A grouping strategy. This is gonna tell the moduleFeed how to group the tiles
    # in different rows
    #
    # - The type of tiles that we are going to support. Each type of tile needs a point,
    # category and an option viewName that reference a Backbone view
    #
    # - defaultView is a reference to a Backbone view that is gonna used to render tiles
    # that doesn't have a viewName asociated
    #
    # - layoutStrategies. Array of Layout strategies that is gonna tell the moduleFeed how
    # to display the grouped tiles
    moduleFeed = new window.ngk.plugins.TileArrangement(tilesCollection.toJSON(), {
        groupingStrategy : window.ngk.utils.NGKGroupingRules,
        tileTypes        : [
            {
                points: 50
                category : "challenge"
                viewName : TileView
            },
            {
                points: 75
                category : "photo"
                viewName : TileView
            },
            {
                points: 25
                category : "poll"
                viewName : TileView
            },
            {
                points: 'wildcard'
                category : "did-you-know"
                viewName : TileView
            }
        ],
        defaultView      : TileView
        layoutStrategies : [
            { points : 75,         layoutClass : window.ngk.utils.HightlightLayoutStrategy }
            { points : 50,         layoutClass : window.ngk.utils.ChallengeLayoutStrategy }
            { points : 25,         layoutClass : window.ngk.utils.PollLayoutStrategy }
            { points : 'wildcard', layoutClass : window.ngk.utils.WildcardLayoutStrategy }
        ]
    })

    # Arrange the tiles according to the grouping and filling rules and return and obj containing the
    # ordered tiles
    orderedTiles = moduleFeed.arrange()

    # apply ordering logic to the rows to re-arrange them based on a passed function
    # In this case, we are shuffleing the rows
    shuffledRows = moduleFeed.applyRowOrderingLogic _.shuffle, orderedTiles

    # put the ordered in the screen
    # the first parameter is an array of ordered rows and the second the view that
    # is gonna represent the row
    moduleFeed.generateViews(shuffledRows, RowContainer)

