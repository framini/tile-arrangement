###*
 * Hightlight Strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class window.ngk.utils.HightlightLayoutStrategy extends window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy

    # this method will allow us to set all the filling rules for mobile
    setMobileRules: () ->

    # this method will allow us to set all the filling rules for desktop
    setDesktopRules: () ->
        if @rowLength == 2
            # in this case we'll have a 25pts tile (100 height) as the second element of the row
            # polls needs to have a special threatment since they are size-fixed

            @_desktopRules = [
                    styles :
                        "width": "100%",
                        "height": "100%"
                ,
                    styles :
                        "margin-right": "-250px",
                        "float": "right",
                        "height": "29.375rem",
                        "width": "250px"
            ]

            @_rowDesktopRules =
                    "height"    : "29.375rem"
                    "padding-right" : "250px"

        else if @rowLength > 2

            @_desktopRules = [
                    styles :
                        "width"       : "75%"
                        "height"      : "100%"
                ,
                    styles :
                        "width"       : "25%"
                        "height"      : "50%"
                ,
                    styles :
                        "width"       : "25%"
                        "height"      : "50%"
            ]

            @_rowDesktopRules =
                    "height"    : "29.375rem"

