###*
 * Poll Strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class window.ngk.utils.PollLayoutStrategy extends window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy

    # this method will allow us to set all the filling rules for mobile
    setMobileRules: () ->

    # this method will allow us to set all the filling rules for desktop
    setDesktopRules: () ->
        # 25pts + 25pts + 25pts + 25pts
        if @_rowElements[1].type == "poll" && @rowLength == 4 # TODO: change this to the generic grouping prop

            @_desktopRules =
                styles :
                    "width"  : "25%"
                    "height" : "29.375rem"

            @_rowDesktopRules =
                "height"    : "29.375rem"

        # 25pts + 25pts + wilcard
        else if @_rowElements[1].type == "poll" && @rowLength == 3

            @_desktopRules = [
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-250px"
                ,
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-500px"
                ,
                    styles :
                        "width"       : "100%"
                        "height"      : "100%"
            ]

            @_rowDesktopRules =
                "height"    : "29.375rem"
                "padding-left" : "500px"

        # 25pts + 25pts + 25pts + 2 wilcards
        else if @rowLength == 5

            @_desktopRules = [
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-250px"
                ,
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-500px"
                ,
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-750px"
                ,
                    styles :
                        "width"       : "100%"
                        "height"      : "50%"
                ,
                    styles :
                        "width"       : "100%"
                        "height"      : "50%"
            ]

            @_rowDesktopRules =
                "height"    : "29.375rem"
                "padding-left" : "750px"


        # 25pts + 3 wildcards
        else

            @_desktopRules = [
                    styles :
                        "width"       : "250px"
                        "height"      : "29.375rem"
                        "margin-left" : "-250px"
                ,
                    styles :
                        "width"       : "70%"
                        "height"      : "100%"
                ,
                    styles :
                        "width"       : "30%"
                        "height"      : "50%"
                ,
                    styles :
                        "width"       : "30%"
                        "height"      : "50%"
            ]

            @_rowDesktopRules =
                "height"        : "29.375rem"
                "padding-left"  : "250px"

