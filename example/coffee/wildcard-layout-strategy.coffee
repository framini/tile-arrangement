###*
 * Wildcard Strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class window.ngk.utils.WildcardLayoutStrategy extends window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy

    # this method will allow us to set all the filling rules for mobile
    setMobileRules: () ->

    # this method will allow us to set all the filling rules for desktop
    setDesktopRules: () ->
        # by default the layouts for rows filled by wildcards is one line
        # with the percentage of each tile proportional to the ammount of tiles
        if @rowLength > 0 && @rowLength <= 4

            # 2 tiles -> 50% each tile, etc
            widthProp = 100 / @rowLength

            # properties thar are gonna be applied to each tile
            @_desktopRules =
                    styles :
                        "width"  : widthProp + "%"
                        "height" : "100%"

            # properties that are gonna be applied to the row container
            if @rowLength == 2
                @_rowDesktopRules =
                    "height"    : "29.375rem" # height of the entire row
            else
                @_rowDesktopRules =
                    "height"    : "14.6875rem"

        # in this case we put one big tile and 4 1x1
        else if @rowLength == 5

            @_desktopRules = [
                    styles :
                        width       : "50%" # instead of picking the first object as the bigger, we could use some criteria to determine it
                        height      : "100%"
                ,
                    styles :
                        width       : "25%"
                        height      : "50%"
                ,
                    styles :
                        width       : "25%"
                        height      : "50%"
                ,
                    styles :
                        width       : "25%"
                        height      : "50%"
                ,
                    styles :
                        width       : "25%"
                        height      : "50%"
            ]

            @_rowDesktopRules =
                "height"    : "29.375rem"

