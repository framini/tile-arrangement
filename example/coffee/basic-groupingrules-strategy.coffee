###*
 * Grouping rules for NGK
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
###

class window.ngk.utils.NGKGroupingRules extends window.ngk.plugins.TileArrangement.ConcreteGroupingRulesStrategy

    ###*
     * Grouping rules for desktop
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###

    setDesktopRules : () ->
        @orderedRow = @_fillRowEmptySpaceDesktop(
            remainingPoints : @remainingPoints,
            row : @row
            wildcardLimitPerRow : @wildcardLimitPerRow
        )

    _fillRowEmptySpaceDesktop : (options) ->

        defaults =
            iteration : 0
            allowTwoQuarters : true
            wildcardLimitPerRow : 3

        opt = {}

        _.extend(opt, defaults, options)

        # nos fijamos en population si hay algun elemento con el espacio que necesitamos
        if @population[opt.remainingPoints] && @population[opt.remainingPoints].length > 0

            # chequeamos si el opt.remainingPoints viene de un wildcard
            if typeof opt.remainingPoints == "string"
                # si entramos aca tenemos que completar la fila completa con wildcards
                # Arrancamos con el caso mas simple de grouping rule, 3 wildcards por fila

                item = @_grabElement opt.remainingPoints
                opt.row.push item

                if opt.row.length < opt.wildcardLimitPerRow
                    @_fillRowEmptySpaceDesktop
                        remainingPoints : 'wildcard'
                        row : opt.row
                        wildcardLimitPerRow : opt.wildcardLimitPerRow

            else
                # Si entramos aca completamos la fila
                # ¿tendriamos que tener en cuenta que no haya por ejemplo, 2 challenges en la misma fila?
                # en caso q tengamos q hacerlo, aca es donde lo tendriamos que trabajar
                item = @_grabElement opt.remainingPoints
                opt.row.push item

        else
            if @population['wildcard'].length > 0

                if opt.remainingPoints <= 25

                    item = @_grabElement 'wildcard'
                    opt.row.push item

                    # grouping rule para tiles de tipo highlight
                    # en caso que no se llene con un poll, se tiene que llenar con 2 regulares
                    # NOTA: quizas convenga sacar estas logicas a sus propias funciones
                    if opt.iteration == 0
                        @_fillRowEmptySpaceDesktop
                            remainingPoints : 25
                            row : opt.row
                            iteration : 1

                else if opt.remainingPoints == 50
                    # Estamos en el caso de un challenge
                    # Para challenges podemos tener varias grouping rules (la opcion challenge + challenge entra en el primer if)
                    # Opcion 1:
                    # 1 challenge + 2 poll
                    #
                    # Opcion 2:
                    # 1 challenge + 1 poll + 2 widlcards de 1x1
                    #
                    # Opcion 3:
                    # 1 challenge + 1 wildcard
                    # NOTA: quizas convenga sacar estas logicas a sus propias funciones

                    #chequeamos si hay polls disponibles
                    if @population[25].length > 0 && opt.allowTwoQuarters
                        # grouping rules para tiles de tipo challenge

                        item = @_grabElement 25
                        opt.row.push item

                        # en el caso que haya 2 polls disponibles, tomamos las 2
                        if opt.iteration == 0 && @population[25].length > 0
                            @_fillRowEmptySpaceDesktop
                                remainingPoints : 50
                                row : opt.row
                                iteration : 1

                        else if opt.iteration == 0 && @population['wildcard'].length > 0
                            # en este caso necesitariamos 2 comodines de 1x1
                            @_fillRowEmptySpaceDesktop
                                remainingPoints : 50
                                row : opt.row
                                iteration : 1

                    else if @population['wildcard'].length > 0
                        item = @_grabElement 'wildcard'
                        opt.row.push item

                        if opt.iteration == 1
                            @_fillRowEmptySpaceDesktop
                                remainingPoints : 25
                                row : opt.row
                                iteration : 2

                else if opt.remainingPoints == 75
                    # Estamos en el caso de un Poll
                    # Para poll en este punto del algoritmo podes tener 3 grouping rules posibles
                    # Opcion 1:
                    # 1 poll + 1 poll + 1 wildcard
                    #
                    # Opcion 2:
                    # 1 poll + 1 poll + 2 wildcards // no
                    #
                    # Opcion 3:
                    # 1 poll + 3 wildcards

                    #chequeamos si hay polls disponibles
                    if @population[25].length > 0
                        # grouping rules para tiles de tipo challenge

                        item = @_grabElement 25
                        opt.row.push item

                        @_fillRowEmptySpaceDesktop
                            remainingPoints : 50
                            row : opt.row
                            allowTwoQuarters : true

                    else
                        # en caso que no haya polls llenamos con 3 wildcards
                        @_fillRowEmptySpaceDesktop
                            remainingPoints : 'wildcard'
                            row : opt.row
                            wildcardLimitPerRow : 4

            else
                # no quedan mas wildcards =(
                # vamos completando las filas con tiles de puntaje fijo
                if typeof opt.remainingPoints == "number"
                    actualValue = @opt.totalPointsPerRow - opt.remainingPoints
                    remaining = opt.remainingPoints

                    if actualValue == 50
                        actualValue = actualValue - @opt.stepPoints

                    if @population[actualValue].length > 0
                        item = @_grabElement actualValue
                        opt.row.push item
                        remaining = remaining - actualValue

                        @_fillRowEmptySpaceDesktop
                                    remainingPoints : remaining
                                    row : opt.row
                                    allowTwoQuarters : true

        # return the ordered row
        return opt.row

    ###*
     * Mobile grouping rules
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    # setMobileRules : () ->

