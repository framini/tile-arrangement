###*
 * Factory class for layout objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class LayoutStrategyFactory

    instance = null

    class LayoutStrategyFactorySingleton

        constructor : ->

            _.bindAll this, 'setStrategies',
                            'createLayout'

            # object to hold all the strategies
            @strategies = {}

        ###*
         * public method to set the group of strategies that are gonna be used by the library
         * @author Francisco Ramini <francisco.ramini at globant.com>
         * @param  {[Array]} strategies
        ###
        setStrategies : (strategies)->

            if _.isArray strategies
                _.each(strategies, (strategyObj, index) =>
                    @strategies[strategyObj.points] = strategyObj.layoutClass
                )
            else
                throw new Error("The param passed to [LayoutStrategyFactory].setStrategies() must be an array")

            return @

        ###*
         * [createLayout description]
         * @author Francisco Ramini <francisco.ramini at globant.com>
         * @param  {[Number]} firstElementPoints
         * @param  {[Array]} row
         * @return {[LayoutStrategy]}
        ###
        createLayout : (firstElementPoints, row) ->

            if @strategies[firstElementPoints]
                layoutStrategy = new @strategies[firstElementPoints]()

                # Initialize the instance
                layoutStrategy.init(row)

                return layoutStrategy
            else
                throw new Error("The layout with points: " + firstElementPoints + " is not defined.")

    @getInstance : ->
        instance ?= new LayoutStrategyFactorySingleton()

module.exports = LayoutStrategyFactory
