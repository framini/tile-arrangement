###*
 * The purpose of this class is to be the client for our grouping strategy
 * pattern.
 *
 * This is gonna be a singleton since one grouping strategy can be applied
 * at a time
 * @author Francisco Ramini <francisco.ramini at globant.com>
###
class Grouper

    instance = null

    class Grouper

        constructor : (strategy) ->

            _.bindAll this, 'getOrderedRow',
                            'setStrategy'

            @strategy = strategy

        getOrderedRow : () ->

            return @strategy.buildGroupingRules()

        setStrategy : (strategy) ->

            @strategy = strategy

    @getInstance : (strategy) ->

        if instance?

            instance.setStrategy strategy

            return instance
        else

            return instance = new Grouper(strategy)

module.exports = Grouper
