###*
 * Concrete instance of GroupingRulesStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
###

GroupingRulesStrategy = require('./groupingrules__strategy')

class window.ngk.plugins.TileArrangement.ConcreteGroupingRulesStrategy extends GroupingRulesStrategy


module.exports = window.ngk.utils.ConcreteGroupingRulesStrategy
