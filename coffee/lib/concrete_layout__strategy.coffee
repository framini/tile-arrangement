###*
 * Concrete instance of LayoutStrategy that is gonna be used from the outside
 * as a super class
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

LayoutStrategy = require('./layout__strategy')

class window.ngk.plugins.TileArrangement.ConcreteLayoutStrategy extends LayoutStrategy

module.exports = window.ngk.utils.ConcreteLayoutStrategy

