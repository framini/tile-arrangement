###*
 * The purpose of this class is to be the client for our strategy
 * pattern
 * @author Francisco Ramini <francisco.ramini at globant.com>
###
class Layouter

    constructor : (strategy) ->
        _.bindAll this, 'getLayout'

        @strategy = strategy

    getLayout : () ->
        return @strategy.buildLayout()

module.exports = Layouter
