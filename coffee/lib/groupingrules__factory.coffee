###*
 * Factory class for fillingrules objects
 * This class is gonna be treated as a singleton
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

class GroupingRulesFactory

    instance = null

    class GroupingRulesFactorySingleton

        constructor : ->

            _.bindAll this, 'setStrategies',
                            'createGroupingRules'

            # for now we could only have 1 strategy
            @strategy = null

        ###*
         * public method to set the group of strategies that are gonna be used by the library
         * @author Francisco Ramini <francisco.ramini at globant.com>
         * @param  {[GroupingRuleStrategy]} strategy
         * @param  {[String]} platform
        ###
        setStrategies : (strategy)->

            @strategy = strategy

            return @

        ###*
         * [createGroupingRules description]
         * @author Francisco Ramini <francisco.ramini at globant.com>
         * @param  {[Number]} firstElementPoints
         * @param  {[Array]} row
         * @return {[GroupingRuleStrategy]}
        ###
        createGroupingRules : () ->

            if @strategy?
                return new @strategy()
            else
                throw new Error("You need to set a grouping strategy before you can use it.")

    @getInstance : ->
        instance ?= new GroupingRulesFactorySingleton()

module.exports = GroupingRulesFactory

