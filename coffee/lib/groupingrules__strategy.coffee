###*
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
###

# dependencies

Utils = require('./utils')

class GroupingRulesStrategy
    constructor: () ->
        _.bindAll this, 'buildGroupingRules',
                        'setRules',
                        'setDesktopRules',
                        'setMobileRules',
                        '_grabElement',
                        'setCurrentRowValues'


    setCurrentRowValues: (options) ->
        @population          = options.population
        @remainingPoints     = options.remainingPoints
        @row                 = options.row
        @wildcardLimitPerRow = options.wildcardLimitPerRow

        @orderedRow = []

    ###*
     * This method is gonna be used by the client (in the strategy pattern) to
     * run the primary algorithms that applies the ordering logic
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[type]}
    ###
    buildGroupingRules : () ->
        @setRules()

    setRules : () ->

        if Utils.isMobile()
            @setMobileRules()
        else
            @setDesktopRules()

    ###*
     * Abstract method that is gonna be implemented by the subclass
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    setDesktopRules : () ->
        throw new Error("[Class GroupingRulesStrategy] You must override setDesktopRules() in your implementing class in order to provide grouping rules for desktop")

    ###*
     * Abstract method that is gonna be implemented by the subclass
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    setMobileRules : () ->
        throw new Error("[Class GroupingRulesStrategy] You must override setMobileRules() in your implementing class in order to provide grouping rules for mobile devices")

    ###*
     * get an element and delete it from the selected collection
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[String]} index
     * @return {[Object]} item
    ###
    _grabElement : (index) ->
        item = @population[index][0]

        @population[index] = @population[index][1..]

        return item

module.exports = GroupingRulesStrategy
