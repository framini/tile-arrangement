###*
 * The purpose of this class is to be the super class for each strategy
 * @author Francisco Ramini <francisco.ramini at globant.com>
 * @dependencies: modernizr
###

# dependencies

Utils = require('./utils')

class LayoutStrategy
    constructor: (row) ->

        _.bindAll this,
                'getLayout',
                '_buildLayoutRules',
                'init',
                'setRules',
                'buildLayout'

    init : (row) ->

        @_mobileRules = {}
        @_desktopRules = {}
        @_rowDesktopRules = {}
        @_rowMobileRules = {}

        @currentRules = []
        @currentRowRules = []

        @_rowElements = row

        # Default classes that are gonna be applied to each tile
        @_generalTileOptions =
            classes : "tile--dynamic"

        @rowLength = @_rowElements.length

    ###*
     * this method is used by our client class (strategy pattern) to call
     * all the needed method to create the layoyt
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    ###
    buildLayout : () ->

        @setRules()

        # update the layout based on the setted rules
        @currentRules = @_setLayout @_rowElements, @currentRules

        return @getLayout()

    ###*
     * set the rules for the corresponding platform (mobile or desktop)
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    setRules : () ->

        if Utils.isMobile()
            @setMobileRules()
            device = '_mobileRules'
        else
            @setDesktopRules()
            device = '_desktopRules'

        @_buildLayoutRules 0, device

        return @

    ###*
     * abstract method that will be overriden by the subclasses and
     * will allow us to set all the filling rules for mobile
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    setMobileRules: () ->
        throw new Error("[Class LayoutStrategy] You must override setMobileRules() in your implementing class in order to provide grouping rules for desktop")


    ###*
     * abstract method that will be overriden by the subclasses and
     * will allow us to set all the filling rules for desktop
     * @author Francisco Ramini <francisco.ramini at globant.com>
    ###
    setDesktopRules: () ->
        throw new Error("[Class LayoutStrategy] You must override setDesktopRules() in your implementing class in order to provide grouping rules for desktop")

    ###*
     * This method returns an object containing the rules for each tile
     * and another object containing the rules to be applied in the row
     * container
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    ###
    getLayout: () ->
        return {
            tileRules : @currentRules
            rowRules : @_rowDesktopRules #cambiar esto a current
        }

    ###*
     * recursive method to alter the object representing a tile with the properties that
     * will be used to generate the layout (used in the related template files)
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[type]} iteration = 0
     * @return {[type]}
    ###
    _buildLayoutRules : ( iteration = 0, device ) ->
        baseObj =
            _domArrangement : {}

        # here we need to identify which layout to build (mobile or desktop)
        baseObj._domArrangement = _.extend(baseObj._domArrangement, if _.isArray @[device] then @[device][iteration] else @[device])

        @currentRules.push baseObj

        if iteration < @rowLength - 1
            iteration++
            @_buildLayoutRules iteration, device

    ###*
     * this method will be in charge of adding the different layout styles (i.e number of columns,
     * size element, etc) to each tile
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} row
     * @param  {[Array/Object]} tileProps
     * @param  {[Number]} iteration = 0
     * @return {[Array]} rows
    ###
    _setLayout : (row, tileProps, iteration = 0) ->

        _.extend(row[iteration],
            # if an object is passed instead of an array, apply the same object
            # to every tile
            if _.isArray tileProps then tileProps[iteration] else tileProps
            ,
            _generalOptions : @_generalTileOptions
        )

        if iteration < row.length - 1
            iteration++
            @_setLayout row, tileProps, iteration

        else
            return row

module.exports = LayoutStrategy
