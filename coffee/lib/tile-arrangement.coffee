###*
 * The purpose of this class is to arrange a group of tiles
 * in rows according to a set of styling rules defined by the
 * design team, that allows each row to have its own layout.
 * @author Francisco Ramini <francisco.ramini at globant.com>
###

## Global dependencies

Grouper               = require('./grouper')
LayoutStrategyFactory = require('./layout__factory')
GroupingRulesFactory  = require('./groupingrules__factory')
Layouter              = require('./layouter')

###*
 * to avoid the consumer of this library to use the it as a module
 * we'll expose the main class as part of the window obj
###
class window.ngk.plugins.TileArrangement
    ###*
     * constructor method
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} elems   = {}
     * @param  {[Object]} options = {}
    ###
    constructor: (elems = {}, options = {})->

        _.bindAll this,
            'arrange',
            '_arrangeRow',
            '_grabFirstElementRow',
            '_completeRowSpace',
            '_grabElement',
            '_maxWildcardsPerRow',
            '_doDomArrangement',
            '_addDomFillingOptions',
            '_findRandomWildcardLimitPerRow',
            'init',
            '_groupPopulation',
            '_createCateogoryPointsArray',
            'setNewCollection',
            'generateViews'

        # instance obj that is gonna hold the default/options passed to the library
        @opt = {}

        @init elems, options if !_.isEmpty elems

    init : (elems, options) ->

        # Defaults
        defaults =
            wildcardLimitPerRow : 3
            stepPoints          : 25
            totalPointsPerRow   : 100 # ammount of points that a rows needs to be considered completed

        _.extend(@opt, defaults, options)

        # tiles population
        @elems = elems

        # creamos una copia de la poblacion de tiles para poder modificarla a gusto
        @elemsCpy = @elems

        # arreglo de rows que va a servir para renderizar las filas en orden
        @rows = []

        # global object to store layouts
        @layouts = {}

        # get the instance (LayoutStrategyFactory is a singleton) of the factory class to instantiate
        # the corresponding layout object
        @layoutFactory = LayoutStrategyFactory.getInstance()

        # Give the factory a list of strategies to use
        @layoutFactory.setStrategies(@opt.layoutStrategies)

        # get the factory for the grouping
        @groupingFactory = GroupingRulesFactory.getInstance()

        # Give the factory a list of grouping strategies to use
        if !!@opt.groupingStrategy
            @groupingFactory.setStrategies(@opt.groupingStrategy)
        else
            throw new Error("The option groupingStrategy is mandatory")

        # the grouping object is gonna be created based on the setted strategy
        # and there is gonna be only one grouping object created
        @groupingObj = @groupingFactory.createGroupingRules()

        # object that will be on charge of grouping the tiles based on the groupingObj strategy
        @grouper = Grouper.getInstance @groupingObj

        # This object is gonna hold the entire tile population
        @population = {}

        if !!@opt.tileTypes.length
            # creates an array with the values passed into tileTypes
            # ordered as desc
            @_createCateogoryPointsArray()

            # Filter the population and creates the groups based on the passed options
            @_groupPopulation()
        else
            throw new Error("The option tileTypes is mandatory")

    ###*
     * creates an ordered array with the elements passed in the tileTypes
     * options
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Array]}
    ###
    _createCateogoryPointsArray : () ->
        @cateogoryPointsArray = []

        _.each(@opt.tileTypes, (element, index, list) =>
            @cateogoryPointsArray.push element.points
        )

        # add the extra category needed by the library
        @cateogoryPointsArray.push 'wildcard'

        # Order the array as desc
        @cateogoryPointsArray.sort (a,b) -> b - a


    ###*
     * set a new collection after the library has been initialized
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} collection
     * @param  {[Object]} options    = {}
    ###
    setNewCollection : (collection, options = {}) ->
        @init(collection, options)

    ###*
     * Recursive method that is used to initialize the population object that
     * is used to hold the member of each of the passed categories
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} iteration = 0
    ###
    _groupPopulation : (iteration = 0) ->

        groupSettings = @opt.tileTypes[iteration]

        if @population.hasOwnProperty groupSettings.points

            items = _.where(@elemsCpy, type : groupSettings.category)

            _.each items, (element, index) =>
                @population[groupSettings.points].push element

        else
            @population[groupSettings.points] = _.where(@elemsCpy, type : groupSettings.category)

        # update de initial population
        @elemsCpy = _.difference(@elemsCpy, @population[groupSettings.points])

        iteration++

        # check if there is anything left to set in the tilType array
        if !!@opt.tileTypes[iteration]

            @_groupPopulation iteration

        else
            # if not, we are done grouping/filtering the population
            # there remaing of our elements will become our wildcards elements

            # first, we'll check if a wilcard type has been passed in the typeTypes
            # property
            if @population.hasOwnProperty 'wildcard'
                _.each @elemsCpy, (element, index) =>
                    @population['wildcard'].push element
            else
                @population['wildcard'] = @elemsCpy

    ###*
     * this method will be in charge of ordering a group of tiles and will
     * be the "public" method that needs to be called whenever we need to
     * to order a group of tiles
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]} arrays
    ###
    arrange : () ->
        _rows = []
        # first we need to order the collection based in filling criterias
        # (tendria que listar aca las filling rules)
        _rows = @_arrangeRow()

        # Once we have the the collection ordered, we need to do some CSS properties
        # to correctly display the tiles
        _rows = @_doDomArrangement _rows

        # finally we save the ordered collection in our public row obj
        @rows = _rows

        # console.log "======================================"
        # console.log @rows

        return @rows

    ###*
     * [applyRowOrderingLogic description]
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Function]} orderingLogic [description]
     * @param  {[Array]} rows          [description]
     * @return {[Array]}               [description]
    ###
    applyRowOrderingLogic : (orderingLogic, rows) ->
        if _.isFunction orderingLogic
            return orderingLogic(rows)
        else
            throw new Error "The first parameter to [tileArrangement].applyRowOrderingLogic must be a function"

    ###*
     * [generateViews description]
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} orderedRows
     * @param  {[BackboneView]} RowContainer
     * @return {[type]}
    ###
    generateViews : (orderedRows, RowContainer) ->
        # once we have the rows ordered the way we want, lets generate the corresponding views
        _.each(orderedRows, (row, index) =>

            rc = new RowContainer model :
                                    styles : row.props[0]

            _.each(row.row, (tile, tileIndex) =>

                tileType = _.find(@opt.tileTypes, (t) ->
                    tile.type == t.category
                )

                # in case we couldnt found the corresponding view
                # well use the default view
                if typeof tileType == "undefined"
                    tileView = new @opt.defaultView()
                else
                    tileView = new tileType.viewName()


                tileView.model = _.extend(tile, cid : tileView.cid)

                rc.setSubViews(tileView.render().el);
            )

            # render and add the row to the main container
            rc.render()
        )



    ###*
     * this method is in charge of adding the necesary options on each object (tile)
     * to correctly display the row in the browser
     * The returning array return an object with the row and the props that needs to be applied
     * to the container of the row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} tilesCollection
     * @return {[Object]} rows
    ###
    _doDomArrangement : (tilesCollection) ->

        rowProps = []
        rows = []

        _.each(tilesCollection, (row, rowNum) =>
            result = @_addDomFillingOptions(row)

            # rowProps.push result.rowProps
            # rows.push result.row
            rows.push(
                row : result.row
                props : result.rowProps
            )
        )

        # return {
        #     rowProps : rowProps
        #     rows : rows
        # }
        return rows

    ###*
     * this method will be in charge of adding the corresponding
     * properties (css options mostly) to the elems that will allow them to be displayed
     * correctly in the browser
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Array]} row
    ###
    _addDomFillingOptions : (row) ->

        # this array wild hold the options for the entire row
        # e.g margins for the row container, etc
        rowLevelProps = []

        # the filling props will be added based on the type of the first object
        firstElement = row[0]

        # this will tell us the size of the element
        firstElementPoints = firstElement._strengthPoints

        # the layout object is gonna be created based on the points of the first element
        layoutObj = @layoutFactory.createLayout(firstElementPoints, row)

        # creates the client for the strategy
        layouter = new Layouter layoutObj

        # get the layout-rules that is gonna be used to build the entire row
        layoutRules = layouter.getLayout()

        # this object holds the properties for each tile
        tileProps = layoutRules.tileRules

        # this object holds the properties for the containing row
        rowProps = layoutRules.rowRules

        # store row-level rules
        rowLevelProps.push rowProps

        # returns an object containing the row and the properties that are
        # gonna be aplied to the row container
        return {
            row      : tileProps
            rowProps : rowLevelProps
        }

    ###*
     * recursive method to iterate over all of the collections an arrange them
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Array]} rows
    ###
    _arrangeRow : (rows = [], iteration = 0) ->
        _rows = rows || []

        firstElement = @_grabFirstElementRow()

        if firstElement
            row = @_completeRowSpace firstElement
            _rows.push row

            # console.log row
            # console.log "---------------------------"

            iteration++
            @_arrangeRow _rows, iteration
        else
            return _rows

    ###*
     * grab (get and erase an element from a collection) the first element for the row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @return {[Object]}
    ###
    _grabFirstElementRow : (iteration = 0) ->

        if !!@population[ @cateogoryPointsArray[iteration] ].length

            item = @_grabElement @cateogoryPointsArray[iteration]
            @_tagElement item, @cateogoryPointsArray[iteration]

            # we are done here, return the selected item
            return item

        else
            iteration++

            if iteration < @cateogoryPointsArray.length
                # if there are no elements left in this category lets try with
                # the next one
                @_grabFirstElementRow iteration
            else
                # if we get this far it means there are no elements left
                return null

    ###*
     * adds an extra property to the passed element that is gonna be used for
     * the ordering algorithm
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]} item
     * @param  {[Number]} index
     * @return {[Object]} item
    ###
    _tagElement : (item, index) ->
        # usamos 'wildcard' como valor para wildcard para indicar que los mismos pueden valer
        # cualquiera de esos pts segun se necesite
        item._strengthPoints = if typeof index == "number" then index else 'wildcard'

    ###*
     * get an element and delete it from the selected collection
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[String]} index
     * @return {[Object]} item
    ###
    _grabElement : (index) ->
        item = @population[index][0]

        @population[index] = @population[index][1..]

        return item

    ###*
     * check if an element is a wildcard
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]}  element
     * @return {Boolean}
    ###
    _isWildCard : (element) ->
        if typeof element._strengthPoints == "number" then false else true

    ###*
     * finds a random number (between 2 and 5) of wildcards per row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Number]} iteration = 0
     * @return {[Number]}
    ###
    _findRandomWildcardLimitPerRow : (iteration = 0) ->

        wildcardPopulationLength = @population['wildcard'].length + 1

        # in case the wildcard population is below 3, there is no need to continue
        if wildcardPopulationLength <= 3
            return wildcardPopulationLength

        minWilcardPerRow = 2

        maxWilcardPerRow = 5

        randomWilcardLimit = _.random minWilcardPerRow, maxWilcardPerRow

        iterationLimit = 20 # to prevent long loops we define the max number of recursivity to 20

        if iteration < 20
            if wildcardPopulationLength > randomWilcardLimit &&
               wildcardPopulationLength - randomWilcardLimit >= 2 &&
               iteration < 10

                return randomWilcardLimit
            else
                iteration++

                @_findRandomWildcardLimitPerRow iteration
        else
            return minWilcardPerRow

    ###*
     * Determines the maximun amount of wildcards per row
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]}  element
     * @param  {Boolean} isWildCard
     * @return {[Number]}
    ###
    _maxWildcardsPerRow : (element, isWildCard) ->

        # in case we dealing with a row filled with all wildcards
        # we'll add some randomness in the ammount of wildcards per row
        # in order to avoid layout repeatness
        if isWildCard

            randomWilcardLimit = @_findRandomWildcardLimitPerRow()

            return randomWilcardLimit

        else
            return @opt.wildcardLimitPerRow

    ###*
     * fills the remaining row space based on the passed element
     * @author Francisco Ramini <francisco.ramini at globant.com>
     * @param  {[Object]} element
     * @return {[Array]}
    ###
    _completeRowSpace : (element) ->

        # detects if the first element is a wildcard
        isWildCard = @_isWildCard element

        # calculate the number of wildcards per row
        wildcardLimitPerRow = @_maxWildcardsPerRow element, isWildCard

        if typeof element._strengthPoints == "number"
            _spaceToFill = @opt.totalPointsPerRow - element._strengthPoints
        else
            # if is not a number it is a string
            # and it means that we need to fill up the entire row with wildcards
            _spaceToFill = 'wildcard'

        # set to our grouping strategy the values of the current row and population
        @groupingObj.setCurrentRowValues(
            population          : @population
            remainingPoints     : _spaceToFill
            row                 : [element]
            wildcardLimitPerRow : wildcardLimitPerRow
        )

        orderedRow = @grouper.getOrderedRow()

        return orderedRow

module.exports = window.ngk.plugins.TileArrangement

