module.exports = (grunt) ->
    # load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks

    # default paths
    ngkidsConfig =
        rootFolder: './'
        buildFolder: './'
        sassDir: 'sass'
        cssDir: 'css'
        appTemplateDir : 'templates'
        pkg: grunt.file.readJSON 'package.json'
        devUrl: 'localhost'
        port: 9000

    grunt.initConfig
        ngk : ngkidsConfig

        watch:

            coffee:
                files: [
                    '<%= ngk.rootFolder %>coffee/*/*.coffee'
                ]
                tasks: [
                    'coffee:compile',
                    'browserify'
                ]
                options:
                    livereload: true

            js:
                files: ['<%= ngk.rootFolder %>']
                options:
                    livereload: true

            compass:
                files: ['<%= ngk.sassDir %>/**/*.{scss,sass}']
                tasks: ['compass:server']

            css:
                files: ['<%= ngk.cssDir %>/style.css']
                options:
                    livereload: true

            handlebars:
                files: 'example/<%= ngk.appTemplateDir %>/*.handlebars'
                tasks: [
                    'handlebars:compile'
                ]
                options:
                    livereload: true

        coffee:
            compile:
                files:
                  '<%= ngk.rootFolder %>example/js/example.js': ['example/**/*.coffee']
                  '<%= ngk.rootFolder %>dist/arranger.js': ['<%= ngk.rootFolder %>coffee/*/*.coffee']

        coffeelint:
            files:
                src: ['<%= ngk.rootFolder %>/**/*.coffee']
            options:
                'indentation':
                    value: 4
                    level: 'warn'
                'no_trailing_whitespace':
                    level: 'ignore'
                'max_line_length':
                    velue: 120
                    level: 'warn'

        compass:
            compile:
                options:
                    sassDir: '<%= ngk.rootFolder %>',
                    cssDir: '<%= ngk.buildFolder %>',
                    require: 'breakpoint'
            build:
                options:
                    environment: 'production'
            ,
            server:
                options:
                    debugInfo: true

        handlebars:
            compile:
                options:
                    namespace: "JST"
                    processName: ( path ) ->
                        path.replace /(.*\/)|(\.handlebars)/g, ""
                files: [
                    expand: true,
                    flatten: false,
                    cwd: '<%= ngk.rootFolder %>',
                    src: ['**/*.handlebars'],
                    dest: '<%= ngk.buildFolder %>',
                    ext: '.js'
                ]

        open:
            server:
                path: 'http://<%= ngk.devUrl %>:<%= ngk.port %>'

        connect:
            options:
                port: 9000,
                livereload: 35729,
                hostname: 'localhost'
            ,
            livereload:
                options:
                    open: true,
                    base: [
                        '.tmp',
                        '<%= ngk.rootFolder %>'
                    ]

        browserify:
            dist:
                files:
                    'dist/arranger.js': ['coffee/lib/tile-arrangement.coffee', 'coffee/lib/concrete_groupingrules__strategy.coffee', 'coffee/lib/concrete_layout__strategy.coffee']

                options:
                    transform: ['coffeeify']
                    extensions: ['.coffee']

    grunt.registerTask 'compile', [
        'compass:compile'
        'coffee:compile'
        'handlebars:compile'
    ]

    grunt.registerTask 'test', [
        'coffeelint'
    ]

    grunt.registerTask 'server', (target) ->

        grunt.task.run [
            'coffee:compile'
            'browserify'
            'compass:server'
            'handlebars:compile'
            'connect:livereload'
            'watch'
        ]
